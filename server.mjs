import express from "express";
import booksRouter from "./routes/books.mjs";

const app = express();
const port = 3000;

app.use(express.json());

// Set Content-Type Header to JSON:API media type
app.use((_, res, next) => {
	res.set("Content-Type", "application/vnd.api+json");
	next();
});

app.use("/api/books", booksRouter());

// Generic error handler
// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
	const status = err.status ?? 500;
	let message;

	if (status !== 500) {
		console.error(err.message);
		message = err.message;
	} else {
		console.error(err);
		message = "Internal Server Error";
	}

	res.status(status).json({ message });
});

app.listen(port, () => {
	console.log(`Server is listening on port ${port}`);
});
