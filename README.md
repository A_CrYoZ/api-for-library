
# REST API For Library

API for managing and interaction with library.

This API implements JSON:API media type.

API request and response examples you can find below.
> There will be described only specific params returned in the body. 
> For detailed information about JSON:API media type you can look into https://jsonapi.org

**API Participants:**
   - Readers
   - Librarians

**API Activities:**
    
- Searching for a book
- Adding new books
- Moving books
- Taking books
- Reserving books
- Interaction with bookshelves

**API Steps:**
- Searching for a book:
    - A librarian and a reader should know which bookshelf the book is on.
    - The book could be found by the ISBN code or its name.
    - Every book has a unique ID in our system.
    - We can have multiple books that match the criteria of our search, and they can be located on multiple bookshelves.
    - It is possible to search for books by genre or author.
- Adding new books:
    - A librarian should be able to put a new book on a shelf. 
    - A librarian should be able to move a book between shelves.
    - A librarian should be able to get the information on a book using its ID.
- Moving books:
    - A reader should be able to take a book to read it during some time period.
    - A librarian should know which reader took a book.
    - A librarian should know which books have been taken and when they will be returned.
- Reserving books:
    - A reader should be able to reserve a book to read it next if it has been taken by someone else.
    - If a book is already reserved for a specific period of time, we should reject this request.
    - A reader should be able to reject a reservation.
    - A reader should know which books they have reserved.
- Interaction with bookshelves:
    - A librarian and a reader should be able to know what books are located on a shelf. 
    - Every shelf has a unique number, its ID.


## API Reference

#### Search book by ISBN Code

```http
  GET http://localhost:3000/api/books/search/isbn/:isbnCode
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `isbnCode` | `string` | **Required**. Book ISBN Code, e.g. 978-8-3108-1739-6 |

**Response:**
```json
{
    "data": {
        "type": "book",
        "id": 10704893102022,
        "attributes": {
            "isbnCode": "978-7-3999-6376-5",
            "name": "1984",
            "genre": "Dystopian Fiction",
            "author": "George Orwell",
            "status": "taken",
            "reserved": true,
            "takenTimestamp": 1234,
            "toReturnTimestamp": 4321,
            "reservedFromTimestamp": 4321,
            "reservedToTimestamp": 5324
        },
        "relationships": {
            "bookshelf": {
                "data": {
                    "type": "bookshelf",
                    "id": 78094493909546
                }
            },
            "reader": {
                "data": {
                    "type": "reader",
                    "id": 1
                }
            },
            "nextReader": {
                "data": {
                    "type": "reader",
                    "id": 2
                }
            }
        },
        "included": [
            {
                "type": "reader",
                "id": 1,
                "attributes": {
                    "name": "Vlad",
                    "surname": "Tarapata",
                    "role": "reader"
                }
            },
            {
                "type": "reader",
                "id": 2,
                "attributes": {
                    "name": "Bogdan",
                    "surname": "Abuziloae",
                    "role": "reader"
                }
            }
        ]
    }
}
```

**Response Parameters in data object:**

| Parameter | Type     | Description                | Always in body? |
| :-------- | :------- | :------------------------- | :------------------------- |
| `type` | `string` | Type of element. Possible types: `book`, `bookshelf`, `reader` | YES |
| `id` | `integer` | unique id of the subject in the system | YES |
| `data.attributes.isbnCode` | `string` | Books unique ISBN code | YES |
| `data.attributes.name` | `string` | Name of the book | YES |
| `data.attributes.genre` | `string` | Genre of the book | YES |
| `data.attributes.author` | `string` | Name of books author | YES |
| `data.attributes.status` | `string` | Current books status. Possible options: `taken` or `free`. If taken - info about reader will be placed in relationships object and detailed info about reader in `included` object | YES |
| `data.attributes.reserved` | `boolean` | Current books reservation status. If reserved - info about nextReader will be placed in relationships object and detailed info about reader in `included` object | YES |
| `data.attributes.takenTimestamp` | `boolean` | Time stamp which represents time when the book was taken. Provided only if `status` is taken | NO |
| `data.attributes.toReturnTimestamp` | `boolean` | Time stamp which represents time when the book will be returned. Provided only if `status` is taken | NO |
| `data.attributes.reservedFromTimestamp` | `boolean` | Time stamp which represents time since when the book will be taken to read. Provided only if `reserved` is true | NO |
| `data.attributes.reservedToTimestamp` | `boolean` | Time stamp which represents time since when the reservation of the book will be cancelled. Provided only if `reserved` is true| NO |
| `relationships.bookshelf` | `object` | Can be not returned if book doesn't belongs to any bookshelf or has no readers at the time of the request. Always contains type and id in internal `data` object | NO |
| `relationships.reader` | `object` | Can be not returned if book is not being read. Always contains type and id in internal `data` object | NO |
| `relationships.nextReader` | `object` | Can be not returned if book is not reserved by reader. Always contains type and id in internal `data` object | NO |
| `included` | `array` | Can be not returned if the book doesn't have current reader or reader who reserved this book. Always contains type, id and it's attributes `data` object | NO |

Possible Errors:

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 400       | Invalid value isbnCode (`isbnCode`) | Client error | Provided isbnCode doesn't matches with isbnCode v13 |
| 404       | Book with such ISBN not found | Client error | Book with such ISBN Code not found |
| 500       | Internal server error | Server error | Something went wrong on server side |

#### Search books by name

```http
  GET http://localhost:3000/api/books/search/isbn/:isbnCode
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `name` | `string` | **Required**. Book name, e.g. 1984 |

**Response:**

Returns an array of found books

```json
{
    "data": [
        {
            "type": "book",
            "id": 69373596403828,
            "attributes": {
                "isbnCode": "978-7-3999-6376-5",
                "name": "1984",
                "genre": "Dystopian Fiction",
                "author": "George Orwell",
                "status": "free",
                "reserved": false
            },
            "relationships": {
                "bookshelf": {
                    "data": {
                        "type": "bookshelf",
                        "id": 54339005263162
                    }
                }
            }
        },
        {
            "type": "book",
            "id": 20391877698821,
            "attributes": {
                "isbnCode": "978-8-4063-3519-3",
                "name": "1984",
                "genre": "Dystopian Fiction",
                "author": "George Orwell",
                "status": "free",
                "reserved": false
            },
            "relationships": {
                "bookshelf": {
                    "data": {
                        "type": "bookshelf",
                        "id": 16537032674906
                    }
                }
            }
        }
    ]
}
```

**Response Parameters in each data object:**

| Parameter | Type     | Description                | Always in body? |
| :-------- | :------- | :------------------------- | :------------------------- |
| `type` | `string` | Type of element. Possible types: `book`, `bookshelf`, `reader` | YES |
| `id` | `integer` | unique id of the subject in the system | YES |
| `data.attributes.isbnCode` | `string` | Books unique ISBN code | YES |
| `data.attributes.name` | `string` | Name of the book | YES |
| `data.attributes.genre` | `string` | Genre of the book | YES |
| `data.attributes.author` | `string` | Name of books author | YES |
| `data.attributes.status` | `string` | Current books status. Possible options: `taken` or `free`. If taken - info about reader will be placed in relationships object and detailed info about reader in `included` object | YES |
| `data.attributes.reserved` | `boolean` | Current books reservation status. If reserved - info about nextReader will be placed in relationships object and detailed info about reader in `included` object | YES |
| `data.attributes.takenTimestamp` | `boolean` | Time stamp which represents time when the book was taken. Provided only if `status` is taken | NO |
| `data.attributes.toReturnTimestamp` | `boolean` | Time stamp which represents time when the book will be returned. Provided only if `status` is taken | NO |
| `data.attributes.reservedFromTimestamp` | `boolean` | Time stamp which represents time since when the book will be taken to read. Provided only if `reserved` is true | NO |
| `data.attributes.reservedToTimestamp` | `boolean` | Time stamp which represents time since when the reservation of the book will be cancelled. Provided only if `reserved` is true| NO |
| `relationships.bookshelf` | `object` | Can be not returned if book doesn't belongs to any bookshelf or has no readers at the time of the request. Always contains type and id in internal `data` object | NO |
| `relationships.reader` | `object` | Can be not returned if book is not being read. Always contains type and id in internal `data` object | NO |
| `relationships.nextReader` | `object` | Can be not returned if book is not reserved by reader. Always contains type and id in internal `data` object | NO |
| `included` | `array` | Can be not returned if the book doesn't have current reader or reader who reserved this book. Always contains type, id and it's attributes `data` object | NO |

Possible Errors:

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 400       | Invalid value name (`name`) | Client error | Provided incorrect name. Min name length = 2 symbols |
| 404       | Book with such name not found | Client error | Books with such name not found |
| 500       | Internal server error | Server error | Something went wrong on server side |


#### Search books by genre

```http
  GET http://localhost:3000/api/books/search/genre/:genre
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `genre` | `string` | **Required**. Book genre, e.g. Dystopian Fiction |

**Response:**

Returns an array of found books

```json
{
    "data": [
        {
            "type": "book",
            "id": 69373596403828,
            "attributes": {
                "isbnCode": "978-7-3999-6376-5",
                "name": "1984",
                "genre": "Dystopian Fiction",
                "author": "George Orwell",
                "status": "free",
                "reserved": false
            },
            "relationships": {
                "bookshelf": {
                    "data": {
                        "type": "bookshelf",
                        "id": 54339005263162
                    }
                }
            }
        },
        {
            "type": "book",
            "id": 20391877698821,
            "attributes": {
                "isbnCode": "978-8-4063-3519-3",
                "name": "1984",
                "genre": "Dystopian Fiction",
                "author": "George Orwell",
                "status": "free",
                "reserved": false
            },
            "relationships": {
                "bookshelf": {
                    "data": {
                        "type": "bookshelf",
                        "id": 16537032674906
                    }
                }
            }
        }
    ]
}
```

**Response Parameters in each data object:**

| Parameter | Type     | Description                | Always in body? |
| :-------- | :------- | :------------------------- | :------------------------- |
| `type` | `string` | Type of element. Possible types: `book`, `bookshelf`, `reader` | YES |
| `id` | `integer` | unique id of the subject in the system | YES |
| `data.attributes.isbnCode` | `string` | Books unique ISBN code | YES |
| `data.attributes.name` | `string` | Name of the book | YES |
| `data.attributes.genre` | `string` | Genre of the book | YES |
| `data.attributes.author` | `string` | Name of books author | YES |
| `data.attributes.status` | `string` | Current books status. Possible options: `taken` or `free`. If taken - info about reader will be placed in relationships object and detailed info about reader in `included` object | YES |
| `data.attributes.reserved` | `boolean` | Current books reservation status. If reserved - info about nextReader will be placed in relationships object and detailed info about reader in `included` object | YES |
| `data.attributes.takenTimestamp` | `boolean` | Time stamp which represents time when the book was taken. Provided only if `status` is taken | NO |
| `data.attributes.toReturnTimestamp` | `boolean` | Time stamp which represents time when the book will be returned. Provided only if `status` is taken | NO |
| `data.attributes.reservedFromTimestamp` | `boolean` | Time stamp which represents time since when the book will be taken to read. Provided only if `reserved` is true | NO |
| `data.attributes.reservedToTimestamp` | `boolean` | Time stamp which represents time since when the reservation of the book will be cancelled. Provided only if `reserved` is true| NO |
| `relationships.bookshelf` | `object` | Can be not returned if book doesn't belongs to any bookshelf or has no readers at the time of the request. Always contains type and id in internal `data` object | NO |
| `relationships.reader` | `object` | Can be not returned if book is not being read. Always contains type and id in internal `data` object | NO |
| `relationships.nextReader` | `object` | Can be not returned if book is not reserved by reader. Always contains type and id in internal `data` object | NO |
| `included` | `array` | Can be not returned if the book doesn't have current reader or reader who reserved this book. Always contains type, id and it's attributes `data` object | NO |

Possible Errors:

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 400       | Invalid value genre (`genre`) | Client error | Provided incorrect genre. Min genre length = 2 symbols |
| 404       | Book with such genre not found | Client error | Books with such genre not found |
| 500       | Internal server error | Server error | Something went wrong on server side |


#### Search books by author

```http
  GET http://localhost:3000/api/books/search/author/:atuhor
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `author` | `string` | **Required**. Book author name, e.g. George Orwell |

**Response:**

Returns an array of found books

```json
{
    "data": [
        {
            "type": "book",
            "id": 69373596403828,
            "attributes": {
                "isbnCode": "978-7-3999-6376-5",
                "name": "1984",
                "genre": "Dystopian Fiction",
                "author": "George Orwell",
                "status": "free",
                "reserved": false
            },
            "relationships": {
                "bookshelf": {
                    "data": {
                        "type": "bookshelf",
                        "id": 54339005263162
                    }
                }
            }
        },
        {
            "type": "book",
            "id": 20391877698821,
            "attributes": {
                "isbnCode": "978-8-4063-3519-3",
                "name": "1984",
                "genre": "Dystopian Fiction",
                "author": "George Orwell",
                "status": "free",
                "reserved": false
            },
            "relationships": {
                "bookshelf": {
                    "data": {
                        "type": "bookshelf",
                        "id": 16537032674906
                    }
                }
            }
        }
    ]
}
```

**Response Parameters in each data object:**

| Parameter | Type     | Description                | Always in body? |
| :-------- | :------- | :------------------------- | :------------------------- |
| `type` | `string` | Type of element. Possible types: `book`, `bookshelf`, `reader` | YES |
| `id` | `integer` | unique id of the subject in the system | YES |
| `data.attributes.isbnCode` | `string` | Books unique ISBN code | YES |
| `data.attributes.name` | `string` | Name of the book | YES |
| `data.attributes.genre` | `string` | Genre of the book | YES |
| `data.attributes.author` | `string` | Name of books author | YES |
| `data.attributes.status` | `string` | Current books status. Possible options: `taken` or `free`. If taken - info about reader will be placed in relationships object and detailed info about reader in `included` object | YES |
| `data.attributes.reserved` | `boolean` | Current books reservation status. If reserved - info about nextReader will be placed in relationships object and detailed info about reader in `included` object | YES |
| `data.attributes.takenTimestamp` | `boolean` | Time stamp which represents time when the book was taken. Provided only if `status` is taken | NO |
| `data.attributes.toReturnTimestamp` | `boolean` | Time stamp which represents time when the book will be returned. Provided only if `status` is taken | NO |
| `data.attributes.reservedFromTimestamp` | `boolean` | Time stamp which represents time since when the book will be taken to read. Provided only if `reserved` is true | NO |
| `data.attributes.reservedToTimestamp` | `boolean` | Time stamp which represents time since when the reservation of the book will be cancelled. Provided only if `reserved` is true| NO |
| `relationships.bookshelf` | `object` | Can be not returned if book doesn't belongs to any bookshelf or has no readers at the time of the request. Always contains type and id in internal `data` object | NO |
| `relationships.reader` | `object` | Can be not returned if book is not being read. Always contains type and id in internal `data` object | NO |
| `relationships.nextReader` | `object` | Can be not returned if book is not reserved by reader. Always contains type and id in internal `data` object | NO |
| `included` | `array` | Can be not returned if the book doesn't have current reader or reader who reserved this book. Always contains type, id and it's attributes `data` object | NO |

Possible Errors:

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 400       | nvalid book author provided: `author` | Client error | Provided incorrect author name. Min author name length = 2 symbols |
| 404       | Book with such author not found | Client error | Books with such author not found |
| 500       | Internal server error | Server error | Something went wrong on server side |

#### Search books by id

```http
  GET http://localhost:3000/api/books/search/bookId/:id
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `id` | `string` | **Required**. Book id, e.g. 52266343963203 |

**Response:**

```json
{
    "data": {
        "type": "book",
        "id": 52266343963203,
        "attributes": {
            "isbnCode": "978-7-3999-6376-5",
            "name": "1984",
            "genre": "Dystopian Fiction",
            "author": "George Orwell",
            "status": "free",
            "reserved": false
        },
        "relationships": {
            "bookshelf": {
                "data": {
                    "type": "bookshelf",
                    "id": 26227605395820
                }
            }
        }
    }
}
```

**Response Parameters in each data object:**

| Parameter | Type     | Description                | Always in body? |
| :-------- | :------- | :------------------------- | :------------------------- |
| `type` | `string` | Type of element. Possible types: `book`, `bookshelf`, `reader` | YES |
| `id` | `integer` | unique id of the subject in the system | YES |
| `data.attributes.isbnCode` | `string` | Books unique ISBN code | YES |
| `data.attributes.name` | `string` | Name of the book | YES |
| `data.attributes.genre` | `string` | Genre of the book | YES |
| `data.attributes.author` | `string` | Name of books author | YES |
| `data.attributes.status` | `string` | Current books status. Possible options: `taken` or `free`. If taken - info about reader will be placed in relationships object and detailed info about reader in `included` object | YES |
| `data.attributes.reserved` | `boolean` | Current books reservation status. If reserved - info about nextReader will be placed in relationships object and detailed info about reader in `included` object | YES |
| `data.attributes.takenTimestamp` | `boolean` | Time stamp which represents time when the book was taken. Provided only if `status` is taken | NO |
| `data.attributes.toReturnTimestamp` | `boolean` | Time stamp which represents time when the book will be returned. Provided only if `status` is taken | NO |
| `data.attributes.reservedFromTimestamp` | `boolean` | Time stamp which represents time since when the book will be taken to read. Provided only if `reserved` is true | NO |
| `data.attributes.reservedToTimestamp` | `boolean` | Time stamp which represents time since when the reservation of the book will be cancelled. Provided only if `reserved` is true| NO |
| `relationships.bookshelf` | `object` | Can be not returned if book doesn't belongs to any bookshelf or has no readers at the time of the request. Always contains type and id in internal `data` object | NO |
| `relationships.reader` | `object` | Can be not returned if book is not being read. Always contains type and id in internal `data` object | NO |
| `relationships.nextReader` | `object` | Can be not returned if book is not reserved by reader. Always contains type and id in internal `data` object | NO |
| `included` | `array` | Can be not returned if the book doesn't have current reader or reader who reserved this book. Always contains type, id and it's attributes `data` object | NO |

Possible Errors:

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 400       | Incorrect bookId `id` | Client error | Provided incorrect book id. Id must be a number |
| 404       | Book with such id not found | Client error | Books with such author not found |
| 500       | Internal server error | Server error | Something went wrong on server side |

#### Search user reservations

```http
  GET http://localhost:3000/api/books/search/reservations/userId/:userId
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `userId` | `string` | **Required**. User id, e.g. 2 |

**Response:**

Returns an array of found books

```json
{
    "data": {
        "type": "book",
        "id": 52266343963203,
        "attributes": {
            "isbnCode": "978-7-3999-6376-5",
            "name": "1984",
            "genre": "Dystopian Fiction",
            "author": "George Orwell",
            "status": "free",
            "reserved": true,
            "reservedFromTimestamp": 4321,
            "reservedToTimestamp": 5324
        },
        "relationships": {
            "bookshelf": {
                "data": {
                    "type": "bookshelf",
                    "id": 26227605395820
                }
            },
            "nextReader": {
                "data": {
                    "type": "reader",
                    "id": 2
                }
            }
        },
        "included": [
            {
                "type": "reader",
                "id": 2,
                "attributes": {
                    "name": "Bogdan",
                    "surname": "Abuziloae",
                    "role": "reader"
                }
            }
        ]
    }
}
```

**Response Parameters in each data object:**

| Parameter | Type     | Description                | Always in body? |
| :-------- | :------- | :------------------------- | :------------------------- |
| `type` | `string` | Type of element. Possible types: `book`, `bookshelf`, `reader` | YES |
| `id` | `integer` | unique id of the subject in the system | YES |
| `data.attributes.isbnCode` | `string` | Books unique ISBN code | YES |
| `data.attributes.name` | `string` | Name of the book | YES |
| `data.attributes.genre` | `string` | Genre of the book | YES |
| `data.attributes.author` | `string` | Name of books author | YES |
| `data.attributes.status` | `string` | Current books status. Possible options: `taken` or `free`. If taken - info about reader will be placed in relationships object and detailed info about reader in `included` object | YES |
| `data.attributes.reserved` | `boolean` | Current books reservation status. If reserved - info about nextReader will be placed in relationships object and detailed info about reader in `included` object | YES |
| `data.attributes.takenTimestamp` | `boolean` | Time stamp which represents time when the book was taken. Provided only if `status` is taken | NO |
| `data.attributes.toReturnTimestamp` | `boolean` | Time stamp which represents time when the book will be returned. Provided only if `status` is taken | NO |
| `data.attributes.reservedFromTimestamp` | `boolean` | Time stamp which represents time since when the book will be taken to read. | YES |
| `data.attributes.reservedToTimestamp` | `boolean` | Time stamp which represents time since when the reservation of the book will be cancelled. | YES |
| `relationships.bookshelf` | `object` | Can be not returned if book doesn't belongs to any bookshelf or has no readers at the time of the request. Always contains type and id in internal `data` object | NO |
| `relationships.reader` | `object` | Can be not returned if book is not being read. Always contains type and id in internal `data` object | NO |
| `relationships.nextReader` | `object` | Can be not returned if book is not reserved by reader. Always contains type and id in internal `data` object | NO |
| `included` | `array` | In this request always returned as the book is reseerved. If the book is being read - will contaion info about current reader. Always contains type, id and it's attributes `data` object | YES |

Possible Errors:

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 400       | Incorrect userId `userId` | Client error | Provided incorrect user id. User id must be a number |
| 404       | User not found | Client error | User with such id not found |
| 404       | Reservations not found | Client error | This user doesn't have any reservations |
| 500       | Internal server error | Server error | Something went wrong on server side |


#### Get all books placed to a specific bookshelf

```http
  GET http://localhost:3000/api/books/search/bookshelf/shelfId/:shelfId
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `shelfId` | `string` | **Required**. Shelf id, e.g. 19586637794402 |

**Response:**

Returns an array of found books

```json
{
    "data": [
        {
            "type": "book",
            "id": 26485625707494,
            "attributes": {
                "isbnCode": "978-7-1155-7469-5",
                "name": "To Kill a Mockingbird",
                "genre": "Classic",
                "author": "Harper Lee",
                "status": "free",
                "reserved": false
            },
            "relationships": {
                "bookshelf": {
                    "data": {
                        "type": "bookshelf",
                        "id": 19586637794402
                    }
                }
            }
        },
        {
            "type": "book",
            "id": 61953696128108,
            "attributes": {
                "isbnCode": "978-8-4063-3519-3",
                "name": "1984",
                "genre": "Dystopian Fiction",
                "author": "George Orwell",
                "status": "free",
                "reserved": false
            },
            "relationships": {
                "bookshelf": {
                    "data": {
                        "type": "bookshelf",
                        "id": 19586637794402
                    }
                }
            }
        },
        {
            "type": "book",
            "id": 54656204854806,
            "attributes": {
                "isbnCode": "978-1-2030-3084-1",
                "name": "The Lord of the Rings",
                "genre": "Fantasy",
                "author": "J.R.R. Tolkien",
                "status": "free",
                "reserved": false
            },
            "relationships": {
                "bookshelf": {
                    "data": {
                        "type": "bookshelf",
                        "id": 19586637794402
                    }
                }
            }
        }
    ]
}
```

**Response Parameters in each data object:**

| Parameter | Type     | Description                | Always in body? |
| :-------- | :------- | :------------------------- | :------------------------- |
| `type` | `string` | Type of element. Possible types: `book`, `bookshelf`, `reader` | YES |
| `id` | `integer` | unique id of the subject in the system | YES |
| `data.attributes.isbnCode` | `string` | Books unique ISBN code | YES |
| `data.attributes.name` | `string` | Name of the book | YES |
| `data.attributes.genre` | `string` | Genre of the book | YES |
| `data.attributes.author` | `string` | Name of books author | YES |
| `data.attributes.status` | `string` | Current books status. Possible options: `taken` or `free`. If taken - info about reader will be placed in relationships object and detailed info about reader in `included` object | YES |
| `data.attributes.reserved` | `boolean` | Current books reservation status. If reserved - info about nextReader will be placed in relationships object and detailed info about reader in `included` object | YES |
| `data.attributes.takenTimestamp` | `boolean` | Time stamp which represents time when the book was taken. Provided only if `status` is taken | NO |
| `data.attributes.toReturnTimestamp` | `boolean` | Time stamp which represents time when the book will be returned. Provided only if `status` is taken | NO |
| `data.attributes.reservedFromTimestamp` | `boolean` | Time stamp which represents time since when the book will be taken to read. | YES |
| `data.attributes.reservedToTimestamp` | `boolean` | Time stamp which represents time since when the reservation of the book will be cancelled. | YES |
| `relationships.bookshelf` | `object` | Can be not returned if book doesn't belongs to any bookshelf or has no readers at the time of the request. Always contains type and id in internal `data` object | NO |
| `relationships.reader` | `object` | Can be not returned if book is not being read. Always contains type and id in internal `data` object | NO |
| `relationships.nextReader` | `object` | Can be not returned if book is not reserved by reader. Always contains type and id in internal `data` object | NO |
| `included` | `array` | In this request always returned as the book is reseerved. If the book is being read - will contaion info about current reader. Always contains type, id and it's attributes `data` object | YES |

Possible Errors:

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 400       | Incorrect shelfId `shelfId` | Client error | Provided incorrect shelf id. Shelf id must be a number |
| 404       | Bookshelf doesn't have books | Client error | Bookshelf doesn't have books |
| 404       | Bookshelf with such id not found | Client error | Ther is no bookshelf with such id |
| 500       | Internal server error | Server error | Something went wrong on server side |

#### Add book to a shelf

```http
  POST http://localhost:3000/api/books/addBook/shelf/id/:shelfId
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `shelfId` | `string` | **Required**. Shelf id, e.g. 19586637794402 |

Request Body params:
| Parameter | Type     | Description                | Data example |
| :-------- | :------- | :------------------------- | :--------- |
| `isbnCode` | `string` | **Required**. ISBN Code of book. Version: 13. | 978-5-7093-0528-1 |
| `name` | `string` | **Required**. Book name | Some book |
| `genre` | `string` | **Required**. Book genre | Novel |
| `author` | `string` | **Required**. Author name | James Bond |

**Response:**

```json
{
    "data": {
        "result": "ok"
    }
}
```

**Response Parameters in data object:**

| Parameter | Type     | Description                | Always in body? |
| :-------- | :------- | :------------------------- | :------------------------- |
| `result` | `string` | Always ok if request is successfull. Otherwise see errors below | YES |

Possible Errors related to body structure:

Errors related to body structure are dynamic.

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 400       | Invalid genre parameter. Invalid author parameter. | Client error | Provided incorrect body params |

Example of error:

```json
{
    "message": "Invalid genre parameter. Invalid author parameter."
}
```

Body provided to reproduce this error:
```json
{
    "isbnCode": "978-2-1421-0239-2",
    "name": "Some book"
}
```

Other Errors:

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 404       | Bookshelf with such id not found | Client error | There is no bookshelf with such id |
| 500       | Internal server error | Server error | Something went wrong on server side |


#### Move book to another shelf

```http
  PATCH http://localhost:3000/api/books/moveBook/bookId/:bookId
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `bookId` | `string` | **Required**. Book id E.g. 26485625707494 |

Request Body params:
| Parameter | Type     | Description                | Data example |
| :-------- | :------- | :------------------------- | :--------- |
| `newBookshelfId` | `number` | **Required**. Bookshelf id on which book to be moved to | 19586637794402 |

**Response:**

```json
{
    "data": {
        "result": "ok"
    }
}
```

**Response Parameters in data object:**

| Parameter | Type     | Description                | Always in body? |
| :-------- | :------- | :------------------------- | :------------------------- |
| `result` | `string` | Always ok if request is successfull. Otherwise see errors below | YES |

Possible Errors related to provided info syntax:

Errors related to request structure are dynamic.

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 400       | Incorrect bookId. Incorrect bookshelfId. | Client error | Provided incorrect body and/or path params |

Example of error:

```json
{
    "message": "Incorrect bookId. Incorrect bookshelfId."
}
```

Body provided to reproduce this error:

```
bookId: sdas
```

```json
{
    "newBookshelfId": "dasd"
}
```

Other Errors:

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 404       | Book or bookshelf with such id not found | Client error | There is no bookshelf or book with such id |
| 500       | Internal server error | Server error | Something went wrong on server side |

#### Assign book to a reader

```http
  PUT http://localhost:3000/api/books/assignReader/userId/:userId
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `userId` | `string` | **Required**. User id E.g. 1 |

Request Body params:
| Parameter | Type     | Description                | Data example |
| :-------- | :------- | :------------------------- | :--------- |
| `bookId` | `number` | **Required**. Book id | 19586637794402 |
| `takenTimestamp` | `number` | **Required**. Timestamp since when reader took book | 19586637794402 |
| `toReturnTimestamp` | `number` | **Required**. Timestamp  when reader must return book | 19586637794402 |

**Response:**

```json
{
    "data": {
        "result": "ok"
    }
}
```

**Response Parameters in data object:**

| Parameter | Type     | Description                | Always in body? |
| :-------- | :------- | :------------------------- | :------------------------- |
| `result` | `string` | Always ok if request is successfull. Otherwise see errors below | YES |

Possible Errors related to provided info syntax:

Errors related to request structure are dynamic.

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 400       | Incorrect takenTimestamp. | Client error | Provided incorrect body and/or path params |

Example of error:

```json
{
    "message": "Incorrect takenTimestamp."
}
```

Body provided to reproduce this error:

```json
{
    "bookId": 60539672606677,
    "takenTimestamp": "sdas",
    "toReturnTimestamp": 4321
}
```

Other Errors:

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 400       | Book already taken | Client error | This book is already in use |
| 404       | Book not found | Client error | There is no book with such id |
| 404       | User not found | Client error | There is no user with such id |
| 500       | Internal server error | Server error | Something went wrong on server side |

#### Reserve book for a reader

```http
  PUT http://localhost:3000/api/books/reserveForReader/userId/:userId
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `userId` | `string` | **Required**. User id E.g. 1 |

Request Body params:
| Parameter | Type     | Description                | Data example |
| :-------- | :------- | :------------------------- | :--------- |
| `bookId` | `number` | **Required**. Book id | 19586637794402 |
| `reservedFromTimestamp` | `number` | **Required**. Timestamp since when reader needs book to be reserved | 19586637794402 |
| `reservedToTimestamp` | `number` | **Required**. Timestamp  when reservation expires | 19586637794402 |

**Response:**

```json
{
    "data": {
        "result": "ok"
    }
}
```

**Response Parameters in data object:**

| Parameter | Type     | Description                | Always in body? |
| :-------- | :------- | :------------------------- | :------------------------- |
| `result` | `string` | Always ok if request is successfull. Otherwise see errors below | YES |

Possible Errors related to provided info syntax:

Errors related to request structure are dynamic.

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 400       | Incorrect reservedToTimestamp. | Client error | Provided incorrect body and/or path params |

Example of error:

```json
{
    "message": "Incorrect reservedToTimestamp."
}
```

Body provided to reproduce this error:

```json
{
    "bookId": 60539672606677,
    "reservedToTimestamp": "sdas",
    "reservedFromTimestamp": 4321
}
```

Other Errors:

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 400       | Reservation time cannot be less then return time | Client error | If you try to reserve book on time less than reader will return it - it will cause this error |
| 400       | Book already reserved | Client error | The book can only have one reservation |
| 404       | Book not found | Client error | There is no book with such id |
| 404       | User not found | Client error | There is no user with such id |
| 500       | Internal server error | Server error | Something went wrong on server side |

#### Cancel book reservation

```http
  PUT http://localhost:3000/api/books/reserveForReader/userId/:userId
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `userId` | `string` | **Required**. User id E.g. 1 |

Request Body params:
| Parameter | Type     | Description                | Data example |
| :-------- | :------- | :------------------------- | :--------- |
| `bookId` | `number` | **Required**. Book id | 19586637794402 |

**Response:**

```json
{
    "data": {
        "result": "ok"
    }
}
```

**Response Parameters in data object:**

| Parameter | Type     | Description                | Always in body? |
| :-------- | :------- | :------------------------- | :------------------------- |
| `result` | `string` | Always ok if request is successfull. Otherwise see errors below | YES |

Possible Errors related to provided info syntax:

Errors related to request structure are dynamic.

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 400       | Incorrect bookId. `bookId`. | Client error | Provided incorrect body and/or path params |

Example of error:

```json
{
    "message": "Incorrect bookId. \"dsa\""
}
```

Body provided to reproduce this error:

```json
{
    "bookId": "dsa"
}
```

Other Errors:

| HTTP Code | Message | Type     | Description                       |
| :-------- | :------ | :------- | :-------------------------------- |
| 400       | This book reserved by another user | Client error | Returns if you try to cancel reservation not related to provided user |
| 400       | Book is not reserved | Client error | The book is not in reserve |
| 404       | Book not found | Client error | There is no book with such id |
| 404       | User not found | Client error | There is no user with such id |
| 500       | Internal server error | Server error | Something went wrong on server side |