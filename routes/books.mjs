import { Router } from "express";
import { param, check, validationResult } from "express-validator";
import createError from "http-errors";

import BooksService from "../services/books-service.mjs";
import JsonApiFormatter from "../services/json-api-formatter.mjs";

import Book from "../modules/book.mjs";

const router = new Router();

export default () => {
	// Get book by ISBN code
	router.get(
		"/search/isbn/:isbnCode",
		param("isbnCode").isISBN({ version: 13 }),
		(req, res, next) => {
			try {
				const errors = validationResult(req);

				if (!errors.isEmpty()) {
					const { msg, path, value } = errors.array()[0];
					next(createError(400, `${msg} ${path} (${value})`));
				} else {
					const book = BooksService.findBookByISBN(req.params.isbnCode);

					if (!book) {
						next(createError(404, "Book with such ISBN not found"));
					} else {
						const response = JsonApiFormatter.FormatBooks(book);

						res.json(response);
					}
				}
			} catch (error) {
				next(error);
			}
		}
	);

	// Get book by name
	router.get(
		"/search/name/:name",
		param("name")
			.trim()
			.isLength({ min: 2 })
			.escape()
			.withMessage("Invalid book name provided:"),
		(req, res, next) => {
			try {
				const errors = validationResult(req);

				if (!errors.isEmpty()) {
					const { msg, value } = errors.array()[0];
					next(createError(400, `${msg} "${value}"`));
				} else {
					const books = BooksService.findBookByName(req.params.name);

					if (books.length === 0) {
						next(createError(404, "Book with such name not found"));
					} else {
						const response = JsonApiFormatter.FormatBooks(books);

						res.json(response);
					}
				}
			} catch (error) {
				next(error);
			}
		}
	);

	// get book by genre
	router.get(
		"/search/genre/:genre",
		param("genre")
			.trim()
			.isLength({ min: 2 })
			.escape()
			.withMessage("Invalid book genre provided:"),
		(req, res, next) => {
			try {
				const errors = validationResult(req);

				if (!errors.isEmpty()) {
					const { msg, value } = errors.array()[0];
					next(createError(400, `${msg} "${value}"`));
				} else {
					const books = BooksService.findBookByGenre(req.params.genre);

					if (books.length === 0) {
						next(createError(404, "Book with such genre not found"));
					} else {
						const response = JsonApiFormatter.FormatBooks(books);

						res.json(response);
					}
				}
			} catch (error) {
				next(error);
			}
		}
	);

	// get book by author
	router.get(
		"/search/author/:author",
		param("author")
			.trim()
			.isLength({ min: 2 })
			.escape()
			.withMessage("Invalid book author provided:"),
		(req, res, next) => {
			try {
				const errors = validationResult(req);

				if (!errors.isEmpty()) {
					const { msg, value } = errors.array()[0];
					next(createError(400, `${msg} "${value}"`));
				} else {
					const books = BooksService.findBookByAuthor(req.params.author);

					if (books.length === 0) {
						next(createError(404, "Book with such author not found"));
					} else {
						const response = JsonApiFormatter.FormatBooks(books);

						res.json(response);
					}
				}
			} catch (error) {
				next(error);
			}
		}
	);

	// get book by id
	router.get(
		"/search/bookId/:bookId",
		param("bookId")
			.isNumeric({ no_symbols: true })
			.withMessage("Incorrect bookId"),
		(req, res, next) => {
			try {
				const errors = validationResult(req);

				if (!errors.isEmpty()) {
					const { msg, value } = errors.array()[0];
					next(createError(400, `${msg} "${value}"`));
				} else {
					const book = BooksService.findBookById(Number(req.params.bookId));

					if (!book) {
						next(createError(404, "Book with such id not found"));
					} else {
						const response = JsonApiFormatter.FormatBooks(book);

						res.json(response);
					}
				}
			} catch (error) {
				next(error);
			}
		}
	);

	// get user reservations
	router.get(
		"/search/reservations/userId/:userId",
		param("userId")
			.isNumeric({ no_symbols: true })
			.withMessage("Incorrect userId"),
		(req, res, next) => {
			try {
				const errors = validationResult(req);

				if (!errors.isEmpty()) {
					const { msg, value } = errors.array()[0];
					next(createError(400, `${msg} "${value}"`));
				} else {
					const books = BooksService.findUserReservations(
						Number(req.params.userId)
					);

					if (books.length === 0) {
						next(createError(404, "Reservations not found"));
					} else {
						const response = JsonApiFormatter.FormatBooks(books);

						res.json(response);
					}
				}
			} catch (error) {
				if (error.message === "User not found") {
					next(createError(404, error.message));
				} else {
					next(error);
				}
			}
		}
	);

	// get all books on shelf
	router.get(
		"/search/bookshelf/shelfId/:shelfId",
		param("shelfId")
			.isNumeric({ no_symbols: true })
			.withMessage("Incorrect shelfId"),
		(req, res, next) => {
			try {
				const errors = validationResult(req);

				if (!errors.isEmpty()) {
					const { msg, value } = errors.array()[0];
					next(createError(400, `${msg} "${value}"`));
				} else {
					const books = BooksService.getAllBooksOnShelf(
						Number(req.params.shelfId)
					);

					if (books.length === 0) {
						next(createError(404, "Bookshelf doesn't have books"));
					} else {
						const response = JsonApiFormatter.FormatBooks(books);

						res.json(response);
					}
				}
			} catch (error) {
				if (error.message === "Bookshelf with such id not found") {
					next(createError(400, error.message));
				} else {
					next(error);
				}
			}
		}
	);

	// add book to shelf
	router.post(
		"/addBook/shelf/id/:id",
		[
			param("id").isNumeric({ no_symbols: true }).withMessage("Incorrect id."),
			check("isbnCode")
				.isISBN({ version: 13 })
				.withMessage("Invalid ISBN code."),
			check("name")
				.trim()
				.isLength({ min: 2 })
				.escape()
				.withMessage("Invalid name parameter."),
			check("genre")
				.trim()
				.isLength({ min: 2 })
				.escape()
				.withMessage("Invalid genre parameter."),
			check("author")
				.trim()
				.isLength({ min: 2 })
				.escape()
				.withMessage("Invalid author parameter."),
		],
		(req, res, next) => {
			try {
				const errors = validationResult(req);

				if (!errors.isEmpty()) {
					const errs = errors.array();
					let errMessages = "";

					errs.forEach((err) => {
						errMessages += `${err.msg} `;
					});

					next(createError(400, errMessages.trim()));
				} else {
					const result = BooksService.addBookToABookshelf(
						new Book(
							req.body.isbnCode,
							req.body.name,
							req.body.genre,
							req.body.author
						),
						Number(req.params.id)
					);

					if (!result) {
						next(createError(404, "Bookshelf with such id not found"));
					} else {
						const response = {
							data: {
								result: "ok",
							},
						};

						res.json(response);
					}
				}
			} catch (error) {
				next(error);
			}
		}
	);

	// move book to another shelf
	router.patch(
		"/moveBook/bookId/:bookId",
		[
			param("bookId")
				.isNumeric({ no_symbols: true })
				.withMessage("Incorrect bookId."),
			check("newBookshelfId")
				.isNumeric({ no_symbols: true })
				.withMessage("Incorrect bookshelfId."),
		],
		(req, res, next) => {
			try {
				const errors = validationResult(req);

				if (!errors.isEmpty()) {
					const errs = errors.array();
					let errMessages = "";

					errs.forEach((err) => {
						errMessages += `${err.msg} `;
					});

					next(createError(400, errMessages.trim()));
				} else {
					const result = BooksService.moveBook(
						Number(req.params.bookId),
						Number(req.body.newBookshelfId)
					);

					if (!result) {
						next(createError(404, "Book or bookshelf with such id not found"));
					} else {
						const response = {
							data: {
								result: "ok",
							},
						};

						res.json(response);
					}
				}
			} catch (error) {
				next(error);
			}
		}
	);

	// assign book to reader
	router.put(
		"/assignReader/userId/:userId",
		[
			param("userId")
				.isNumeric({ no_symbols: true })
				.withMessage("Incorrect userId."),
			check("bookId")
				.isNumeric({ no_symbols: true })
				.withMessage("Incorrect bookId."),
			check("takenTimestamp")
				.isNumeric({ no_symbols: true })
				.withMessage("Incorrect takenTimestamp."),
			check("toReturnTimestamp")
				.isNumeric({ no_symbols: true })
				.withMessage("Incorrect toReturnTimestamp."),
		],
		(req, res, next) => {
			try {
				const errors = validationResult(req);

				if (!errors.isEmpty()) {
					const errs = errors.array();
					let errMessages = "";

					errs.forEach((err) => {
						errMessages += `${err.msg} `;
					});

					next(createError(400, errMessages.trim()));
				} else {
					BooksService.assignToReader(
						Number(req.params.userId),
						Number(req.body.bookId),
						Number(req.body.takenTimestamp),
						Number(req.body.toReturnTimestamp)
					);

					const response = {
						data: {
							result: "ok",
						},
					};

					res.json(response);
				}
			} catch (error) {
				if (error.message === "Book already taken") {
					next(createError(400, error.message));
				} else if (
					error.message === "Book not found" ||
					error.message === "User not found"
				) {
					next(createError(404, error.message));
				} else {
					next(error);
				}
			}
		}
	);

	// reserve book to reader
	router.put(
		"/reserveForReader/userId/:userId",
		[
			param("userId")
				.isNumeric({ no_symbols: true })
				.withMessage("Incorrect userId."),
			check("bookId")
				.isNumeric({ no_symbols: true })
				.withMessage("Incorrect bookId."),
			check("reservedFromTimestamp")
				.isNumeric({ no_symbols: true })
				.withMessage("Incorrect reservedFromTimestamp."),
			check("reservedToTimestamp")
				.isNumeric({ no_symbols: true })
				.withMessage("Incorrect reservedToTimestamp."),
		],
		(req, res, next) => {
			try {
				const errors = validationResult(req);

				if (!errors.isEmpty()) {
					const errs = errors.array();
					let errMessages = "";

					errs.forEach((err) => {
						errMessages += `${err.msg} `;
					});

					next(createError(400, errMessages.trim()));
				} else {
					BooksService.reserveForReader(
						Number(req.params.userId),
						Number(req.body.bookId),
						Number(req.body.reservedFromTimestamp),
						Number(req.body.reservedToTimestamp)
					);

					const response = {
						data: {
							result: "ok",
						},
					};

					res.json(response);
				}
			} catch (error) {
				if (
					error.message ===
						"Reservation time cannot be less then return time" ||
					error.message === "Book already reserved"
				) {
					next(createError(400, error.message));
				} else if (
					error.message === "Book not found" ||
					error.message === "User not found"
				) {
					next(createError(404, error.message));
				} else {
					next(error);
				}
			}
		}
	);

	// cancel reservation
	router.put(
		"/cancelReservation/userId/:userId",
		[
			check("bookId")
				.isNumeric({ no_symbols: true })
				.withMessage("Incorrect bookId."),
		],
		(req, res, next) => {
			try {
				const errors = validationResult(req);

				if (!errors.isEmpty()) {
					const { msg, value } = errors.array()[0];
					next(createError(400, `${msg} "${value}"`));
				} else {
					BooksService.cancelReservation(
						Number(req.params.userId),
						Number(req.body.bookId)
					);

					const response = {
						data: {
							result: "ok",
						},
					};

					res.json(response);
				}
			} catch (error) {
				if (
					error.message === "This book reserved by another user" ||
					error.message === "Book is not reserved"
				) {
					next(createError(400, error.message));
				} else if (
					error.message === "Book not found" ||
					error.message === "User not found"
				) {
					next(createError(404, error.message));
				} else {
					next(error);
				}
			}
		}
	);

	return router;
};
