/* eslint-disable no-unused-vars */
import allBooks from "../utils/all-books.mjs";

import Book from "../modules/book.mjs";
import Bookshelf from "../modules/bookshelf.mjs";
import User from "../modules/user.mjs";

// * Fill our library with test data. In real tasks it's not working this way, it works with database

// #region test-data
// Init bookshelves
const bookshelf1 = new Bookshelf();
const bookshelf2 = new Bookshelf();
const bookshelf3 = new Bookshelf();

// Bookshelf with unique books
bookshelf1.addMultipleBooks([
	new Book(
		"978-3-1327-1912-5",
		"To Kill a Mockingbird",
		"Classic",
		"Harper Lee"
	),
	new Book("978-7-3999-6376-5", "1984", "Dystopian Fiction", "George Orwell"),
	new Book(
		"978-5-1024-0039-2",
		"The Lord of the Rings",
		"Fantasy",
		"J.R.R. Tolkien"
	),
]);

// Bookshelf with same books as bookshelf 1 but with other ISBNs
bookshelf2.addMultipleBooks([
	new Book(
		"978-7-1155-7469-5",
		"To Kill a Mockingbird",
		"Classic",
		"Harper Lee"
	),
	new Book("978-8-4063-3519-3", "1984", "Dystopian Fiction", "George Orwell"),
	new Book(
		"978-1-2030-3084-1",
		"The Lord of the Rings",
		"Fantasy",
		"J.R.R. Tolkien"
	),
]);

// Bookshelf which has other books written by George Orwell
bookshelf3.addMultipleBooks([
	new Book(
		"978-6-4452-6797-0",
		"Animal Farm",
		"Political Allegory",
		"George Orwell"
	),
	new Book(
		"978-4-3691-9938-4",
		"Homage to Catalonia",
		"Memoir",
		"George Orwell"
	),
	new Book("978-7-5056-1603-5", "Burmese Days", "Historical", "George Orwell"),
]);

const bookshelves = [bookshelf1, bookshelf2, bookshelf3];

// Create some users
const users = [
	new User(1, "Vlad", "Tarapata", "reader"),
	new User(2, "Bogdan", "Abuziloae", "reader"),
	new User(3, "Helen", "Dark", "librarian"),
];

// #endregion

class BooksService {
	static #findBookshelfById(id) {
		return bookshelves.find((item) => item.getId === id);
	}

	static #findUserById(id) {
		return users.find((item) => item.getId === id);
	}

	static findBookByISBN(isbn) {
		return allBooks.find((book) => book.getISBN === isbn);
	}

	static findBookByName(name) {
		return allBooks.filter((book) => book.getName === name);
	}

	static findBookByGenre(genre) {
		return allBooks.filter((book) => book.getGenre === genre);
	}

	static findBookByAuthor(author) {
		return allBooks.filter((book) => book.getAuthor === author);
	}

	static findBookById(id) {
		return allBooks.find((book) => book.getId === id);
	}

	static findUserReservations(userId) {
		const user = this.#findUserById(userId);

		if (user) {
			return allBooks.filter((book) => book.getReservedBy?.getId === userId);
		}
		throw new Error("User not found");
	}

	static getAllBooksOnShelf(shelfId) {
		const bookshelf = bookshelves.find((item) => item.getId === shelfId);

		if (bookshelf) {
			return bookshelf.getBooks;
		}
		throw new Error("Bookshelf with such id not found");
	}

	static addBookToABookshelf(book, bookshelfId) {
		const bookshelf = this.#findBookshelfById(bookshelfId);

		if (bookshelf) {
			bookshelf.addBook(book);
			return true;
		}
		return false;
	}

	static moveBook(bookId, newShelfId) {
		const book = this.findBookById(bookId);

		if (book) {
			const bookshelf = this.#findBookshelfById(newShelfId);

			if (bookshelf) {
				book.setBookshelf = newShelfId;
				return true;
			}
		}
		return false;
	}

	static assignToReader(userId, bookId, takenTimestamp, toReturnTimestamp) {
		const user = this.#findUserById(userId);

		if (user) {
			const book = this.findBookById(bookId);

			if (book) {
				if (book.getStatus === "free") {
					book.takeBook(user, takenTimestamp, toReturnTimestamp);
					return;
				}
				throw new Error("Book already taken");
			}
			throw new Error("Book not found");
		}
		throw new Error("User not found");
	}

	static reserveForReader(
		userId,
		bookId,
		reservedFromTimestamp,
		reservedToTimestamp
	) {
		const user = this.#findUserById(userId);

		if (user) {
			const book = this.findBookById(bookId);

			if (book) {
				if (!book.getReserved) {
					if (book.getToReturnTimestamp <= reservedFromTimestamp) {
						book.reserve(user, reservedFromTimestamp, reservedToTimestamp);
						return;
					}
					throw new Error("Reservation time cannot be less then return time");
				}
				throw new Error("Book already reserved");
			}
			throw new Error("Book not found");
		}
		throw new Error("User not found");
	}

	static cancelReservation(userId, bookId) {
		const user = this.#findUserById(userId);

		if (user) {
			const book = this.findBookById(bookId);

			if (book) {
				if (book.getReserved) {
					if (book.getReservedBy.getId === userId) {
						book.cancelReservation();
						return;
					}

					throw new Error("This book reserved by another user");
				}
				throw new Error("Book is not reserved");
			}
			throw new Error("Book not found");
		}
		throw new Error("User not found");
	}
}

export default BooksService;
