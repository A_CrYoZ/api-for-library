const availableTypes = {
	book: "book",
	bookshelf: "bookshelf",
	reader: "reader",
};

class JsonApiFormatter {
	static #getBookInfoObj(book) {
		const bookInfo = book.getInfo();

		const bookObject = {
			type: availableTypes.book,
			id: bookInfo.id,
			attributes: {
				isbnCode: bookInfo.isbnCode,
				name: bookInfo.name,
				genre: bookInfo.genre,
				author: bookInfo.author,
				status: bookInfo.status,
				reserved: bookInfo.reserved,
			},
		};

		if (bookInfo.bookshelfID) {
			bookObject.relationships = {
				bookshelf: {
					data: {
						type: availableTypes.bookshelf,
						id: bookInfo.bookshelfID,
					},
				},
			};
		}

		if (bookInfo.status === "taken") {
			bookObject.attributes.takenTimestamp = bookInfo.takenTimestamp;
			bookObject.attributes.toReturnTimestamp = bookInfo.toReturnTimestamp;

			const userInfo = bookInfo.reader.getUserDescription;

			if (!bookObject.relationships) bookObject.relationships = {};

			bookObject.relationships.reader = {
				data: {
					type: availableTypes.reader,
					id: userInfo.id,
				},
			};

			bookObject.included = [
				{
					type: availableTypes.reader,
					id: userInfo.id,
					attributes: {
						name: userInfo.name,
						surname: userInfo.surname,
						role: userInfo.role,
					},
				},
			];
		}

		if (bookInfo.reserved) {
			bookObject.attributes.reservedFromTimestamp =
				bookInfo.reservedFromTimestamp;

			bookObject.attributes.reservedToTimestamp = bookInfo.reservedToTimestamp;

			const userInfo = bookInfo.reservedBy.getUserDescription;

			if (!bookObject.relationships) bookObject.relationships = {};

			bookObject.relationships.nextReader = {
				data: {
					type: availableTypes.reader,
					id: userInfo.id,
				},
			};

			if (!bookObject.included) bookObject.included = [];

			bookObject.included.push({
				type: availableTypes.reader,
				id: userInfo.id,
				attributes: {
					name: userInfo.name,
					surname: userInfo.surname,
					role: userInfo.role,
				},
			});
		}
		return bookObject;
	}

	static FormatBooks(books) {
		const isArray = Array.isArray(books);
		// Initialize response object
		const response = {};
		if (isArray && books.length > 1) {
			const data = [];

			books.forEach((book) => {
				const bookInfoObject = this.#getBookInfoObj(book);

				data.push(bookInfoObject);
			});

			response.data = data;
		} else {
			const book = isArray ? books[0] : books;
			const bookInfoObject = this.#getBookInfoObj(book);

			response.data = bookInfoObject;
		}

		return response;
	}
}

export default JsonApiFormatter;
