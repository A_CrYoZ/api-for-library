// Store information about all books we create
// It's not good practice to store all in memory, but for HW is enough.
// In real case we would use a database to manipulate this data and it would be much simpler to get data you need with
// SQL queries.
const allBooks = [];

export default allBooks;
