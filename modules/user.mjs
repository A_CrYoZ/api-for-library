class User {
	#id;

	constructor(id, name, surname, role) {
		this.#id = id;
		this.name = name;
		this.surname = surname;
		this.role = role;
	}

	get getUserDescription() {
		return {
			id: this.#id,
			name: this.name,
			surname: this.surname,
			role: this.role,
		};
	}

	get getId() {
		return this.#id;
	}
}

export default User;
