class Bookshelf {
	#id;

	constructor() {
		this.#id = Math.floor(Date.now() * Math.random() * 50);
		this.books = [];
	}

	addBook(book) {
		// eslint-disable-next-line no-param-reassign
		book.setBookshelf = this.#id;
		this.books.push(book);
	}

	addMultipleBooks(books) {
		books.forEach((book) => {
			// eslint-disable-next-line no-param-reassign
			book.setBookshelf = this.#id;
			this.books.push(book);
		});
	}

	get getId() {
		return this.#id;
	}

	get getBooks() {
		return this.books;
	}
}

export default Bookshelf;
