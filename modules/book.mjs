import allBooks from "../utils/all-books.mjs";

class Book {
	//  #region private class members
	#id; // Unique book ID

	#isbnCode; // Unique ISBN Code

	#name;

	#genre;

	#author;

	#bookshelfID;

	#status;

	#reader;

	#takenTimestamp;

	#toReturnTimestamp;

	#reserved;

	#reservedBy;

	#reservedFromTimestamp;

	#reservedToTimestamp;
	// #endregion

	constructor(isbnCode, name, genre, author) {
		if (!isbnCode && !name && !genre && !author)
			throw new Error("isbnCode, name, genre and author are mandatory fields!");
		this.#id = Math.floor(Date.now() * Math.random() * 50);
		this.#isbnCode = isbnCode;
		this.#name = name;
		this.#genre = genre;
		this.#author = author;
		this.#bookshelfID = null;
		this.#status = "free";
		this.#reader = null;
		this.#takenTimestamp = null;
		this.#toReturnTimestamp = null;
		this.#reserved = false;
		this.#reservedBy = null;
		this.#reservedFromTimestamp = null;
		this.#reservedToTimestamp = null;
		allBooks.push(this);
	}

	getInfo() {
		return {
			id: this.#id,
			isbnCode: this.#isbnCode,
			name: this.#name,
			genre: this.#genre,
			author: this.#author,
			bookshelfID: this.#bookshelfID,
			status: this.#status,
			reader: this.#reader,
			takenTimestamp: this.#takenTimestamp,
			toReturnTimestamp: this.#toReturnTimestamp,
			reserved: this.#reserved,
			reservedBy: this.#reservedBy,
			reservedFromTimestamp: this.#reservedFromTimestamp,
			reservedToTimestamp: this.#reservedToTimestamp,
		};
	}

	get getISBN() {
		return this.#isbnCode;
	}

	get getName() {
		return this.#name;
	}

	get getGenre() {
		return this.#genre;
	}

	get getAuthor() {
		return this.#author;
	}

	get getId() {
		return this.#id;
	}

	get getStatus() {
		return this.#status;
	}

	get getReserved() {
		return this.#reserved;
	}

	get getReservedBy() {
		return this.#reservedBy;
	}

	get getToReturnTimestamp() {
		return this.#toReturnTimestamp;
	}

	takeBook(user, takenTimestamp, toReturnTimestamp) {
		this.#reader = user;
		this.#takenTimestamp = takenTimestamp;
		this.#toReturnTimestamp = toReturnTimestamp;
		this.#status = "taken";
	}

	returnBook() {
		this.#reader = null;
		this.#takenTimestamp = null;
		this.#toReturnTimestamp = null;
		this.#status = "free";
	}

	reserve(user, reservedFromTimestamp, reservedToTimestamp) {
		this.#reserved = true;
		this.#reservedBy = user;
		this.#reservedFromTimestamp = reservedFromTimestamp;
		this.#reservedToTimestamp = reservedToTimestamp;
	}

	cancelReservation() {
		this.#reserved = false;
		this.#reservedBy = null;
		this.#reservedFromTimestamp = null;
		this.#reservedToTimestamp = null;
	}

	/**
	 * @param {Integer} bookshelfID
	 */
	set setBookshelf(bookshelfID) {
		this.#bookshelfID = bookshelfID;
	}
}

export default Book;
